Juntoku Shimizu - jshimiz1@binghamton.edu
Matt Price - mprice3@binghamton.edu
Java
Bingsuns

How to execute program:
Unzip, and in the directory above the project folder, run
    java Project.Bank.Bank <bank port>
    java Project.Psystem.Psystem <listen port> <domain> <bank port> 
    java Project.Customer.Customer <domain> <psystem port>

Encryption/Decryption Code
Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
cipher.init(Cipher.ENCRYPT_MODE, privKey);
byte[] encryptedPrice = cipher.doFinal(priceString.getBytes("UTF-8"));
**Decryption is the same but with DECRYPT_MODE in the init call

The directory RSAKeys contains the java program used to generate the public/private key pairs
Occasionally an incorrect hashed password will be hashed to something with a newline which will send the password in two separate socket data packets, resulting in an error.

Sources used: 
    http://www.sha1-online.com/sha1-java/ (for SHA1 in Java)
    http://stackoverflow.com/questions/18571223/how-to-convert-java-string-into-byte
    http://www.javamex.com/tutorials/cryptography/rsa_encryption.shtml
    https://docs.oracle.com/javase/tutorial/security/apisign/step3.html
    http://stackoverflow.com/questions/4580982/javax-crypto-badpaddingexception 
    http://stackoverflow.com/questions/24915753/java-program-to-verify-digital-signature-signed-by-signtool
