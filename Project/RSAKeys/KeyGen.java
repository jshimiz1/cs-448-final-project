import javax.crypto.*;
import java.security.*;
import java.io.*;
import java.security.spec.*;
import java.math.BigInteger;

public class KeyGen {
    public static void main(String[] args) throws Exception {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair kp = keyGen.genKeyPair();
        /*Key publicKey = keys.getPublic();
        Key privateKey = keys.getPrivate();
        */
        KeyFactory fact = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec pub = fact.getKeySpec(kp.getPublic(),
                  RSAPublicKeySpec.class);
        RSAPrivateKeySpec priv = fact.getKeySpec(kp.getPrivate(),
                  RSAPrivateKeySpec.class);

        saveToFile("public.key", pub.getModulus(),
                  pub.getPublicExponent());
        saveToFile("private.key", priv.getModulus(),
                  priv.getPrivateExponent());
    }
    
    public static void saveToFile(String fileName,
              BigInteger mod, BigInteger exp) throws IOException {
        ObjectOutputStream oout = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(fileName)));
        try {
            oout.writeObject(mod);
            oout.writeObject(exp);
        } catch (Exception e) {
            throw new IOException("Unexpected error", e);
        } finally {
            oout.close();
        }
    }
}
