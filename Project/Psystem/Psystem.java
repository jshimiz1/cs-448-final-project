package Project.Psystem;
import javax.crypto.*;
import java.util.*;
import java.io.*;
import Project.Customer.Customer;
import java.net.*;
import java.security.*;
import java.security.spec.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Base64;
import javax.xml.bind.DatatypeConverter;
//format:
//java Psystem <purchasing-system-port> <bank-ip> <bank-port> 
public class Psystem {
	public static void main(String[] args) throws Exception {
		
		if(args.length != 3) {
			System.out.println("Incorrect number of arguments!");
			System.out.println("Usage: $java Psystem <purchasing-system-port> <bank-ip> <bank-port>");
			System.exit(1);
		}
		
		//If everything goes well
		int bankPort = Integer.parseInt(args[2]);
		String bankIP = args[1];
		int pSystemPort = Integer.parseInt(args[0]);
		String fromClient;
	    	HashMap<String, String> userInfo = new HashMap<String, String>();

		//reader for "password" file
		BufferedReader br = null;
		
		//listen to client
		ServerSocket listenToCust = new ServerSocket(pSystemPort);
		Socket customerConn = listenToCust.accept();
        System.out.println("Connected to client.");
		Socket bankConn = new Socket("localhost", bankPort);
       	System.out.println("Connected to bank.");
		
		while(true) {
			BufferedReader fromCust = new BufferedReader(new InputStreamReader(customerConn.getInputStream()));
			PrintWriter toCust = new PrintWriter(customerConn.getOutputStream(), true);
			BufferedReader fromBank = new BufferedReader(new InputStreamReader(bankConn.getInputStream()));
			PrintWriter toBank = new PrintWriter(bankConn.getOutputStream(), true);
			
			
			//reads <userid, pass>
            String passLine = "";
			try {
                br = new BufferedReader(new FileReader("Project/Psystem/password"));
                String userName = "";
                while((passLine = br.readLine()) != null) {
                    //check if input stream is being read properly
                    //System.out.println(passLine);
                    String[] tokens = passLine.split(":");
                    if(tokens[0].equals("ID")){
                        userName = tokens[1].trim();
                    } else if (tokens[0].equals("Password")){
                        userInfo.put(userName, tokens[1].trim());
                        //System.out.println("Password: " + tokens[1].trim());
                    }
                }
			} catch (IOException e) {
				System.out.println("IOException was caught while trying to read inputstream");
				System.exit(1);
			}
		    //System.out.println("Done loading userinfo");

			//now compare the user id and hashed password to those stored in "password"
	        boolean authenticate = true;
            String userIn = fromCust.readLine();
            do {
                String password = fromCust.readLine();    
            
                String hashedPass = userInfo.get(userIn);
                if(hashedPass == null){
                    //Send error then exit
                    System.out.println("User: " + userIn + " is not registered");
                    System.exit(1);
                } else {
                    //Check password
                    if(!hashedPass.equals(password)){
                        //Send error then exit
                        toCust.println("error");
                        System.out.println("Incorrect Password!!");
                    } else {
                        authenticate = false;
                        toCust.println("valid");
                        System.out.println("Passwords match!");
                    }
                }
            } while (authenticate);

			
			int flag = 1;
			BufferedReader br2 = null;
			HashMap<Integer, Integer> values = new HashMap<Integer, Integer>();
			if(flag == 1) {
			
				//password is correct
				//send "item" to client
				
				br2 = new BufferedReader(new FileReader("Project/Psystem/item"));
                String line = "";
				
                while((line = br2.readLine()) != null) {
					toCust.println(line);
					StringTokenizer lineTok = new StringTokenizer(line, ", $");
					Integer itemID = Integer.parseInt(lineTok.nextToken());
					String item = lineTok.nextToken();
		
					Integer value = Integer.parseInt(lineTok.nextToken());
					values.put(itemID, value);
				}
               toCust.println("done"); 
			}

            fromCust.readLine();
			String itemNumEncrypted;
			String dsEncrypted;
			String creditCardEncrypted;
			itemNumEncrypted = fromCust.readLine();
		
            //System.out.println("Item Num: " + itemNumEncrypted);

			//decrypt E(Pup, <item#>) using private key
			//of purchasing server
			PrivateKey privKey = readPrivateKeyFromFile("Project/Psystem/private.key");
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, privKey);
			byte[] decryptedItemNum = null;
            try {
                decryptedItemNum = cipher.doFinal(DatatypeConverter.parseBase64Binary(itemNumEncrypted));
            } catch (BadPaddingException e){
                System.out.println(e);
            }
            int decrypted = Integer.parseInt(new String(decryptedItemNum, "UTF-8"));
			int price = values.get(decrypted);
			String priceString = Integer.toString(price);
			
            dsEncrypted = fromCust.readLine();
           // System.out.println("dsEncrypted: " + dsEncrypted);

			//verify the Digital Signature
			String publicKeyFile = "Project/Customer/" + userIn + "Public.key";
			PublicKey userPubKey = readKeyFromFile(publicKeyFile);
			Signature signer = Signature.getInstance("SHA1withRSA");
			signer.initVerify(userPubKey);
			boolean verified =  signer.verify(DatatypeConverter.parseBase64Binary(dsEncrypted));
			if(verified == true) {
				System.out.println("The signature has been verified.");
			}
			else {
				System.out.println("Invalid Digital Signature! Exiting Now...");
			    System.exit(1);
            }

			//send E(pub, <name || credit card number>) to Bank
			creditCardEncrypted = fromCust.readLine();            
            //System.out.println("creditCardEncrypted: " + creditCardEncrypted);
			toBank.println(creditCardEncrypted);
				
			//send E(Prp,price) to the bank	
		    cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");	
			cipher.init(Cipher.ENCRYPT_MODE, privKey);
            byte[] encryptedPrice = cipher.doFinal(priceString.getBytes("UTF-8"));
            toBank.println(DatatypeConverter.printBase64Binary(encryptedPrice));

			String response = fromBank.readLine();
			toCust.println(response);	
			
		    customerConn.close();
		    //bankConn.close();
	        break;
        }
	}			
				 
    public static byte[] fromHexString(String s) {
    int len = s.length();
    byte[] data = new byte[(len / 2) + 1];
    for (int i = 0; i < len; i += 2) {
            if(i != len - 1){
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                             + Character.digit(s.charAt(i+1), 16));
            } else {
                data[i/2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit('\0', 16));
            }
    }
    return data;
    }   			
				
	public static PrivateKey readPrivateKeyFromFile(String keyFileName) throws IOException {
        	FileInputStream in = new FileInputStream(keyFileName);
        	ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
          	try {
              		BigInteger m = (BigInteger) oin.readObject();
              		BigInteger e = (BigInteger) oin.readObject();
              		RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(m, e);
              		KeyFactory fact = KeyFactory.getInstance("RSA");
              		PrivateKey privKey = fact.generatePrivate(keySpec);
              		return privKey;
          	} catch (Exception e) {
              		throw new RuntimeException("Spurious serialisation error", e);
          	} finally {
              		oin.close();
         	}
    	}
    	
    	public static PublicKey readKeyFromFile(String keyFileName) throws IOException {
        FileInputStream in = new FileInputStream(keyFileName);
        ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
          try {
              BigInteger m = (BigInteger) oin.readObject();
              BigInteger e = (BigInteger) oin.readObject();
              RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
              KeyFactory fact = KeyFactory.getInstance("RSA");
              PublicKey pubKey = fact.generatePublic(keySpec);
              return pubKey;
          } catch (Exception e) {
              throw new RuntimeException("Spurious serialisation error", e);
          } finally {
              oin.close();
          }
    }
				
		
		}
			
